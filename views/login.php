<?php
	if(!defined('__INCLUDEBSAPP')) {die('Direct access not permitted');}
	$SSO = Vars::$ssourl.'?token='.Vars::$token.'&user='.session_id();
?><!DOCTYPE html>
<!--[if lt IE 7]>	<html class="ie ie6 lte10 lte9 lte8 lte7" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 7]>		<html class="ie ie7 lte10 lte9 lte8" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 8]>		<html class="ie ie8 lte10 lte9" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 9]>		<html class="ie ie9 lte10" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if gt IE 9]>	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if !IE]><!-->	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><!--<![endif]-->
	<head>
		<noscript><meta http-equiv="refresh" content="0; url=noscript.php" /></noscript>
		<!--[if lte IE 9]><meta http-equiv="refresh" content="0; url=incompatible.php" /><![endif]-->
		<title><?php print Vars::$appName; ?></title>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>
		$(function(){
			$.ajax({
				url : "<?php print $SSO; ?>",
				dataType: "json",
			    xhrFields: {
			        withCredentials: true
			    }
			}).done(function(d){
				console.log(d);
				if(!d.status) {
					window.location.reload();
				}
			}).fail(function(d){
				location.href = '<?php print Vars::$ssologin; ?>?callback=<?php
				print urlencode("//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"); ?>';
				/*
				var error = '';	
				if(d.message) error = d.message;
				else if(d.responseJSON && d.responseJSON.message) error = d.responseJSON.message;
				else if(d.responseText) error = d.responseText;
				else error = 'Something bad is happening, we don\'t know what.. :)';
				$('.error p').text(error);
				$('.error').show();
				*/
			});
		});
		</script>
		<style>
			body,html{width:100%;height:100%;margin:0;padding:0;font-family:Arial;}
			.error{margin: 55px;width: 300px;display:none;}
			.error h1{color:red;}
		</style>
	</head>
	<body>
		<div class="error">
			<h1>Error</h1>
			<p></p>
			<br/><br/>
			<a href="<?php print Vars::$ssologin; ?>?callback=<?php print Utils::getSite(true) ?>">Login</a>
		</div>
	</body>
</html>