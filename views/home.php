<?php
	if(!defined('__INCLUDEBSAPP')) {die('Direct access not permitted');}
?><!DOCTYPE html>
<!--[if lt IE 7]>	<html class="ie ie6 lte10 lte9 lte8 lte7" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 7]>		<html class="ie ie7 lte10 lte9 lte8" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 8]>		<html class="ie ie8 lte10 lte9" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if IE 9]>		<html class="ie ie9 lte10" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if gt IE 9]>	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><![endif]-->
<!--[if !IE]><!-->	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" ><!--<![endif]-->
	<head>
		<?php include 'components/head.php'; ?>
	</head>
	<body>
		<div id="wrapper">

	        <!-- Navigation -->
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			    <div class="navbar-header">
			        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			        </button>
			        <a class="navbar-brand" href="/"></a>
			    </div>

			    <ul class="nav navbar-nav navbar-right log-out">
			    	<li title="Log out"><a href="http://app.bridestory.com/app/logout"><?php 
			    	echo isset($_SESSION['user']['fname'])? $_SESSION['user']['fname']: 'Unknown';
			    	?><span class="fa fa-power-off" style="margin-left:13px;margin-right:8px"></span></a></li>
			    </ul>

			    <div class="navbar-default sidebar" role="navigation">
			        <div class="sidebar-nav navbar-collapse">
			        	<?php include 'components/menu.php'; ?>
			        </div>
			        <!-- /.sidebar-collapse -->
			    </div>
			    <!-- /.navbar-static-side -->
			</nav>


	        <div id="loader"></div>
	        <div id="page-wrapper">
	            <div class="container-fluid"></div>
	        </div>
	        <!-- /#page-wrapper -->

	    </div>
	    <!-- /#wrapper -->
	    <?php include 'components/js.php'; ?>
	</body>

	</html>

