<?php
	
	define('__INCLUDEBSAPP',1);

	include '_includes.php';
	
	if(!Auth::check()){ 
		include 'views/login.php';
	}else{
		include 'views/home.php';
	}

?>