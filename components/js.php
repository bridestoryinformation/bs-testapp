<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/cw.minimal.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/cw.plugins.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/bootbox.js"></script>
 <!-- Metis Menu Plugin JavaScript -->
<script src="<?php print Utils::getSite(true); ?>public/javascripts/metisMenu.min.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/routes.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/main.js"></script>
<script src="<?php print Utils::getSite(true); ?>public/javascripts/intranet.js"></script>
