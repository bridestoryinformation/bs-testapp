<?php
	if(!defined('__INCLUDEBSAPP')) {die('Direct access not permitted');}
?>
<noscript><meta http-equiv="refresh" content="0; url=noscript.php" /></noscript>
<!--[if lte IE 9]><meta http-equiv="refresh" content="0; url=incompatible.php" /><![endif]-->
<title><?php print Vars::$appName; ?></title>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script>App={}; App.getSite=function(){return '<?php print Utils::getSite(true); ?>';};</script>
<script>(function(){
	Hash=[];
	App.getRoute = function(url){for(var i in Hash){if(url.match(Hash[i].url)) return Hash[i];} return false;};
	App.setRoute = function(obj){Hash.push(obj)};
	App.apiUrl = function(){
		var apiurl = '<?php print Vars::$apiUrl; ?>';
		if(apiurl.match(/^http(s)?\:\/\//)) return apiurl;
		return App.getSite()+apiurl;
	}
})();</script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css">

<!-- Bootstrap Core CSS -->
<link href="<?php print Utils::getSite(true); ?>public/stylesheets/bootstrap.min.css" rel="stylesheet">

<!-- jQueryMy CSS -->
<link href="<?php print Utils::getSite(true); ?>public/stylesheets/plugin.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?php print Utils::getSite(true); ?>public/stylesheets/metisMenu.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php print Utils::getSite(true); ?>public/stylesheets/font-awesome.min.css" rel="stylesheet" type="text/css">

<link href='//fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/monokai_sublime.min.css">
<link rel="stylesheet" href="<?php print Utils::getSite(true); ?>public/stylesheets/main.css">
<link rel="stylesheet" href="<?php print Utils::getSite(true); ?>public/stylesheets/plugin.css">
<!-- Custom CSS -->
<link href="<?php print Utils::getSite(true); ?>public/stylesheets/intranet.css" rel="stylesheet">