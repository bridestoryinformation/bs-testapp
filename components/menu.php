<?php if(!defined('__INCLUDEBSAPP')) {die('Direct access not permitted');} ?>

<ul class="nav" id="side-menu">
    <!-- if you need image above the menu
    <li class="sidebar-profile-image">
        <a href="#profile"></a>
    </li>
    -->
    <li title="Log out" class="log-out-side-menu">
        <span class="pull-left"><?php 
                    echo isset($_SESSION['user']['fname'])? $_SESSION['user']['fname']: 'Unknown';
                    ?></span>
        <span class="pull-right"><a href="https://app.bridestory.com/app/logout" class="side-menu"><i class="fa fa-power-off fa-fw"></i> Log Out</a></span>
        <span class="clearfix"></span>
    </li>
    <li class="app-name">BS Test App</li>
    <li>
        <a href="#default" class="side-menu"><i class="fa fa-user fa-fw"></i> Home</a>
    </li>
    <li>
        <a href="#docs" class="side-menu"><i class="fa fa-user fa-fw"></i> Docs</a>
    </li>
    <li>
        <a href="#ref" class="side-menu"><i class="fa fa-user fa-fw"></i> Reference</a>
    </li>
    <li>
        <a href="#api" class="side-menu"><i class="fa fa-user fa-fw"></i> API</a>
    </li>
    <li>
        <a href="#" class="side-menu"><i class="fa fa-calendar fa-fw"></i> Menu Sample</a>
        <ul class="nav nav-second-level">
            <li>
                <a href="#submenu1" class="side-menu"><i class="fa fa-briefcase fa-fw"></i> Sub menu 1</a>
            </li>
            <li>
                <a href="#submenu2" class="side-menu"><i class="fa fa-briefcase fa-fw"></i> Sub menu 2</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="https://app.bridestory.com/app" class="side-menu"><i class="fa fa-chevron-left fa-fw"></i> Back to App</a>
    </li>
</ul>