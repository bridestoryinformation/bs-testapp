<?php
	
	if( empty( $_SERVER['HTTP_X_BS_CREDENTIAL'] ) ) die('empty credential');
	if( empty( $_SERVER['HTTP_X_BS_USER'] ) ) die('user token empty');

	set_include_path('classes');
	include 'Vars.php';
	$m = new Memcached();
	$m->addServer( Vars::$memcachehost, Vars::$memcacheport );
	$m->set( $_SERVER['HTTP_X_BS_CREDENTIAL'], $_SERVER['HTTP_X_BS_USER'] );

?>