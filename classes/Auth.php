<?php
	
	class AuthError{
		static $nonactive = 'Please activate your account';
		static $wrong = 'Wrong password or username';
		static $deleted = 'You are currently inactive';
	}

	//deps: DB, Vars
	class Auth{

		static function init(){

			// 8 hours
			ini_set('session.gc_maxlifetime',Vars::$sessionLife);
			session_name(Vars::$sessionName);
			$host = Vars::$host == 'localhost' ? '' : Vars::$host;
			session_set_cookie_params( Vars::$sessionLife, Vars::$root, Vars::$host);
			session_start();
			
		}

		static function check(){
			
			$m = new Memcached();
			$m->addServer(Vars::$memcachehost, Vars::$memcacheport);
			$_SESSION['token'] = $m->get(session_id());

			if(empty($_SESSION['token'])) return false;
			if(!self::reset()) {
				$m->delete(session_id());
				session_regenerate_id(true);
				return false;
			}
			return true;
		}

		static private function reset(){

			require('http_socket.php');
			$http = new http_socket(Vars::$ssoapi.'/user/token/'.$_SESSION['token']);
			$url = parse_url(Vars::$ssoapi);
			$http->setHeader('X-BS-AUTH', md5(Vars::$secret.preg_replace('/\/$/','',$url['path']).'/user/token/'.$_SESSION['token']));
			$http->send();

			if($http->checkStatus(200)){
				$d = json_decode( $http->getResponseBody(), true );
				if(!$d) return false;
				if(!$d['status']) return false;
				$_SESSION['user'] = $d['message'];
				return true;
			}else{
				return false;
			}
			
		}

	}

	Auth::init();

?>