<?php
/*
** copyright Tri Rahmat Gunadi
** v2.0  
*/

	require_once('Socket.php');
	
	define('HEAD',0);
	define('GET',1);
	define('POST',2);
	define('PUT',3);
	define('DELETE',4);
	define('TRACE',5);
	define('OPTIONS',6);
	define('CONNECT',7);
	define('PATCH',8);

class http_socket extends Socket{
	
	private $meths = array('HEAD','GET','POST','PUT','DELETE','TRACE','OPTIONS','CONNECT','PATCH');
	private $method;
	private $content_headers;
	private $content_body;
	private $http_version='1.1';
	private $http_request='';
	private $response_status;
	private $response_headers;
	private $response_body;
	private $follow_redirect = false;
	
	public $compress = false;
	

	public function __construct($url='http://website.com', $meth = 1) {
		$this->reset($url,$meth);
	}

	public function reset($uri=0,$met=1){
		
		if(!$uri) $uri			='http://website.com';
		
		
		// reset response and request parameters
		$this->content_body 	= '';
		$this->response_status	= '';
		$this->response_body	= '';
		$this->response_headers	= array();
		
		$uri 					= parse_url($uri);
		$this->method			= $met;
		$this->path 			= isset($uri['path']) ? $uri['path'] : '/';
		
		if(isset($uri['query'])) $this->path .= '?'.$uri['query'];
		
		$this->setHost($uri['host']);
		$this->setProtocol(isset($uri['scheme']) ? $uri['scheme'] : 'http');
		
		if(!empty($uri['port'])) $this->setPort($uri['port']*1);
		else $this->setPort($uri['scheme'] == 'https' ? 443 : 80);
		
		// reset headers
		$this->content_headers = array();
		$this->content_headers["Host"] 				= $this->host;
		$this->content_headers["User-Agent"] 		= 'JUJI_http_client';
		$this->content_headers["From"]				= isset($_SERVER['SERVER_ADMIN']) ? $_SERVER["SERVER_ADMIN"] : 'jujiyangasli@gmail.com';
		$this->content_headers["Accept"]			= '*/*';
		$this->content_headers["Accept-Charset"]	= 'utf-8';
		$this->content_headers["Accept-Language"]	= 'en';
		$this->content_headers["Accept-Encoding"]	= function_exists('gzdecode') && $this->compress ? 'gzip; q=1.0, identity; q=0.5, *; q=0' : 'identity';
		$this->content_headers["Cache-Control"]		= 'no-cache, no-transform';
		$this->content_headers["Connection"] 		= 'close';
		$this->content_headers["Date"] 				= gmdate('r');
		
		if(isset($_SERVER['HTTP_REFERER']))
		$this->content_headers["Referer"]			= $_SERVER['HTTP_REFERER'];
		
		if(function_exists('gzencode') && $this->compress)
		$this->content_headers["Content-Encoding"]	= 'gzip';
		
	}


	public function setMethod($num) {
		
		if(!is_int($num)) throw new Exception ('http_proxy->setmethod() accepts only integer as argument');
		$this->method = $num;
		
	}

	public function getMethodNum($str){
		return array_search($str,$this->meths);
	}

	public function setHeader($name,$value=false){
		
		if(is_array($name)) {$this->addHeaders($name); return;}
		if(!(is_string($name) && is_string($value))) throw new Exception('setheader($name,$value) accepts string as arguments');
		$this->content_headers[$name]=$value;
		
	}
	
	private function addHeaders($arr){
		
		$err = 'http_socket->addheaders() accepts only array with the format: arr["headerName"] = headerValue';
		if(!is_array($arr)) throw new Exception($err);
		
		foreach($arr as $k=>$v){
			if (!is_string($k)) throw new Exception($err);
			$this->content_headers[$k] = $v;
		}
		
	}
	

	public function setBody($str,$ctype='application/x-www-form-urlencoded'){
		
		if($ctype && is_string($ctype)) $this->content_headers["Content-Type"] = $ctype;
		else throw new Exception ('methods with request body should have a Content-type header (string)');
		
		if (is_array($str)){
			
			$arr = array();
			
			foreach($str as $k => $v){
				if(!is_string($k)) 
				throw new Exception('wrong array type as request body; expected arr["key"] = "value"');
				$arr[] = "$k=$v";
			}
			
			$str = implode('&',$arr);
			$this->content_body = $str;
			
		}elseif(is_string($str)){
			
			$this->content_body = $str;
			
		}else{throw new Exception('http_proxy->setbody() accepts array or string as request body');}
		
		if(function_exists('gzencode')  && $this->compress) $this->content_body = gzencode($this->content_body);
		$this->content_headers["Content-Length"] = strlen($this->content_body);
		
	}
	
	public function setMultipart($form,$files=false){
		
		$bound = 'juji'.time().'hellyeah';
		$poststr = '';
		foreach($form as $k => $v){
			$poststr .= "--$bound\r\n".
			'content-disposition: form-data; name="'.$k.'"'.
			"\r\n\r\n".$v."\r\n";
		}
		
		if(!$files) $files = array();
		foreach($files as $k=>&$v){
			
			if(is_array($v['name'])) {
				$v = $this->diverse_files_array($v);
				foreach($v as $l=>$w){
					
					$poststr .= "--$bound\r\n".'content-disposition: form-data; name="'.
					$k.'[]"; filename="'.basename($w['name']).'"'."\r\n".
					'Content-Type: '.$w['type']."\r\n";
					
					if(isset($w['base64'])) $poststr .= "Content-Transfer-Encoding: base64\r\n\r\n".$w['base64']."\r\n";
					else if(isset($w['binary'])) $poststr .= "Content-Transfer-Encoding: binary\r\n\r\n".$w['binary']."\r\n";
					else $poststr .= "Content-Transfer-Encoding: binary\r\n\r\n".file_get_contents($w['tmp_name'])."\r\n";
				}
			}else{
				
				$poststr .= "--$bound\r\n".'content-disposition: form-data; name="'.
				$k.'"; filename="'.basename($v['name']).'"'."\r\n".
				'Content-Type: '.$v['type']."\r\n";
				
				if(isset($v['base64'])) $poststr .= "Content-Transfer-Encoding: base64\r\n\r\n".$v['base64']."\r\n";
				else if(isset($v['binary'])) $poststr .= "Content-Transfer-Encoding: binary\r\n\r\n".$v['binary']."\r\n";
				else $poststr .= "Content-Transfer-Encoding: binary\r\n\r\n".file_get_contents($v['tmp_name'])."\r\n";
				
			}
			
		}
		
		$poststr .= "--$bound--";
		$this->setbody($poststr,'multipart/form-data; boundary='.$bound);
		
	}


	public function setVersion($str){
		
		$err = 'http_proxy->setversion() accepts only string as argument';
		if(is_string($str)) {$this->version = $str;}
		else { throw new Exception($err); }
		
	}
	
	
	public function setRedirection($r){
		
		if($r) $this->follow_redirect = true;
		else $this->follow_redirect = false;
		
	}


	public function getResponseHeaders (){
		
		return $this->response_headers;
		
	}

	public function getResponseBody (){
		
		return $this->response_body;
		
	}


	public function getResponseStatus (){

		return $this->response_status;
		
	}
	
	public function getSentData(){
		
		$h = array();
		$h['http'] = $this->http_request;
		$h['headers'] = $this->content_headers;
		$h['body'] = $this->content_body;
		$h['connection'] = $this->connection;
		return $h;
		
	}
	
	public function getHttpResponse(){
		return array($this->response_status,$this->response_headers,$this->response_body);
	}
	
	public function debugResponse($xml=0){
		
		$sent = $this->getSentData();
		$response = $this->getHttpResponse();
		
		if($xml) $response[2] = $this->xmlpp($response[2]);
		else{
			$response[2] = htmlentities($response[2]);
		}
		
		print '<pre>';
		print_r($response);
		print '</pre><pre>';
		print_r($sent);
		print '</pre>';
		
	}
	
	
	public function checkStatus($num){
		
		return preg_match('/\s'.$num.'\s/',$this->response_status);
		
	}
	
	
	public function isRedirect(){
		
		return $this->checkStatus(301) ||
		$this->checkStatus(302) ||
		$this->checkStatus(303) ||
		$this->checkStatus(307) ||
		$this->checkStatus(308);
		
	}
	
	
	public function followRedirect(){
		
		if(!isset($this->response_headers['Location'])) throw new Exception('Location header not found.');
		$bd = $this->content_body . '';
		$this->reset($this->response_headers['Location'],$this->method);
		if($bd) $this->content_headers["Content-Length"] = strlen($bd);
		$this->content_body = $bd;
		return $this->send();
		
	}

	public function send(){
		
		if($this->proxy) $this->path = $this->protocol . '://' . $this->content_headers["Host"].$this->path;
		
		//build HTTP request strings
		$this->http_request = $this->meths[$this->method].' '.$this->path.' HTTP/'.$this->http_version."\r\n";
		$mssg = $this->http_request.'';
		foreach($this->content_headers as $k=>$v) $mssg .= $k.': '.$v."\r\n"; 
		$mssg .= "\r\n".$this->content_body;
		$this->setMessage($mssg);
		
		//start connect
		if($this->connect() && parent::send()) $response = $this->getResponse();
		else{
			return false;
		}
		
		$splitpos = strpos($response,"\r\n\r\n");
		$shead = substr($response,0,$splitpos);
		$shead = explode("\r\n",$shead);
		
		//status
		$this->response_status = $shead[0];
		
		//headers
		foreach(range(1,sizeof($shead)-1) as $idx){
			$hv = explode(': ',$shead[$idx]);
			$this->response_headers[$hv[0]] = trim($hv[1]);
		}
		
		if($this->follow_redirect && $this->isRedirect()){
			return $this->followRedirect();
		}
		
		//body
		$this->response_body = substr($response,$splitpos+4);
		$head = $this->response_headers;
		
		//adjustments for contents
		if(isset($head['Transfer-Encoding']) && 
		strtolower($head['Transfer-Encoding'])=='chunked') $this->response_body = $this->unchunkHttp11($this->response_body);
		
		if(isset($head['Content-Encoding']) && 
		strtolower($head['Content-Encoding'])=='gzip') $this->response_body = $this->decompress($this->response_body);
		
		return $response;
	}
	
	private function xmlpp($xml, $html_output=true) {
		// By Eric (Google)
		$xml_obj = new SimpleXMLElement($xml);  
		$level = 4;  
		$indent = 0; // current indentation level  
		$pretty = array();  
		
		// get an array containing each XML element  
		$xml = explode("\n", preg_replace('/>\s*</', ">\n<", $xml_obj->asXML()));  
	
		// shift off opening XML tag if present  
		if (count($xml) && preg_match('/^<\?\s*xml/', $xml[0])) {  
			$pretty[] = array_shift($xml);  
		}  
	
		foreach ($xml as $el) {  
			if (preg_match('/^<([\w])+[^>\/]*>$/U', $el)) {  
				// opening tag, increase indent  
				$pretty[] = str_repeat(' ', $indent) . $el;  
				$indent += $level;  
			} else {  
				if (preg_match('/^<\/.+>$/', $el)) {              
				$indent -= $level;  // closing tag, decrease indent  
				}  
				if ($indent < 0) {  
				$indent += $level;  
				}  
				$pretty[] = str_repeat(' ', $indent) . $el;  
			}  
		}     
		$xml = implode("\n", $pretty);     
		return ($html_output) ? htmlentities($xml) : $xml;  
	}
	
	private function unchunkHttp11($data) {
		$fp = 0;
		$outData = "";
		while ($fp < strlen($data)) {
			$rawnum = substr($data, $fp, strpos(substr($data, $fp), "\r\n") + 2);
			$num = hexdec(trim($rawnum));
			$fp += strlen($rawnum);
			$chunk = substr($data, $fp, $num);
			$outData .= $chunk;
			$fp += strlen($chunk);
		}
		return $outData;
	}
	
	private function decompress( $compressed, $length = null ) {
		if ( function_exists('gzdecode') ) {
			$decompressed = @gzdecode( $compressed );
			if ( false !== $decompressed ) return $decompressed;
		}
		if ( false !== ($decompressed = @gzinflate( $compressed ) ) ) return $decompressed;
		if ( false !== ( $decompressed = $this->compatible_gzinflate( $compressed ) ) ) return $decompressed;
		if ( false !== ( $decompressed = @gzuncompress( $compressed ) ) ) return $decompressed;
		
		return $compressed;
	}

	private function compatible_gzinflate($gzData) {
		if ( substr($gzData, 0, 3) == "\x1f\x8b\x08" ) {
			$i = 10;
			$flg = ord( substr($gzData, 3, 1) );
			if ( $flg > 0 ) {
				if ( $flg & 4 ) {
					list($xlen) = unpack('v', substr($gzData, $i, 2) );
					$i = $i + 2 + $xlen;
				}
				if ( $flg & 8 )
					$i = strpos($gzData, "\0", $i) + 1;
				if ( $flg & 16 )
					$i = strpos($gzData, "\0", $i) + 1;
				if ( $flg & 2 )
					$i = $i + 2;
			}
			return @gzinflate( substr($gzData, $i, -8) );
		} else {
			return false;
		}
	}
	
	private function diverse_files_array($vector) { 
		$result = array(); 
		foreach($vector as $key1 => $value1) 
			foreach($value1 as $key2 => $value2) 
				$result[$key2][$key1] = $value2; 
		return $result; 
	} 
}
?>
