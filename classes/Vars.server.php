<?php

	class Vars{

		// your email
		static $supportmail = 'tri.gunadi@bridestory.com';

		// your app token
		static $token = 'ee5d1838a18af522a3ddfcce0074d1ad';

		// your app secret
		static $secret = '6dd77640cc76e908f6102274a4cb6902d705f348';

		// your app name 
		static $appName = 'BS-Recruit';

		// your session name
		static $sessionName = 'BSRecruitSess';
		static $sessionLife = 14400;

		// Api end-point
		// relative to your base url
		// or start with http
		static $apiUrl = 'api';
		
		// this is needed
		static $memcachehost = 'localhost';
		static $memcacheport = 11211;

		// from this point on, it's up to you
		static $timezone = 'Asia/Jakarta';
		static $GLOBAL = '';
		static $LANG;
		static $cache = false;
		static $developersite = 'app.bridestory.com';

		static $dbhost = '';
		static $dbport = '';
		static $dbname = ''; 
		static $dbuser = ''; 
		static $dbpass = '';

		static $ssourl = 'https://app.bridestory.com/app/auth';
		static $ssoapi = 'https://app.bridestory.com/app/api';
		static $ssologin = 'https://app.bridestory.com/app';

		static $host = '';
		static $root = '';
		static $rootDir = './';
		static $protocol = 'https';

		// do not change, unless you know what you're doing
		static function setRoot($str){
			self::$root = preg_replace('/\/+/','/','/'.$str.'/');
		}
		
		static function inSubdir(){
			$level = self::findRoot();
			self::$root = explode('/',self::$root);
			array_pop(self::$root);
			array_shift(self::$root);
			for($r=0;$r<$level;$r++) array_pop(self::$root);
			self::$root = preg_replace('/\/+/','/','/' . implode('/',self::$root) . '/');
		}
		
		static private function findRoot($num=0){
			$str = './';
			for($r=0;$r<$num;$r++){$str.='../';}
			self::$rootDir = $str;
			return self::isRoot($str) ? $num : self::findRoot(++$num);
		}
		
		static private function isRoot($dir='./'){
			$files = array('classes');
			$ok = true;
			foreach($files as $k=>$v){if(!file_exists($dir.$v)) $ok = false;}
			return $ok;
		}
		
		static function init(){
			//*
			
			//encoding and timezone
			mb_internal_encoding( 'UTF-8' );
			ini_set('date.timezone',self::$timezone);
			date_default_timezone_set(self::$timezone);

			//determine root
			self::$host = $_SERVER['HTTP_HOST'];
			self::$root = str_replace($_SERVER['DOCUMENT_ROOT'],'',preg_replace('/\/[^\/]+$/','',$_SERVER['SCRIPT_FILENAME']));
			self::$root = preg_replace('/\/+/','/','/'.self::$root.'/');
			self::$protocol = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's');
			if(!self::isRoot()) self::inSubdir();

			/**/
			//global vars
			self::$GLOBAL = array();	
			$GLOBAL['website'] = self::$host.self::$root;
			$GLOBAL['mainsite'] = '';
			Vars::$GLOBAL['gpc'] = get_magic_quotes_gpc();

		} 
	}

	Vars::init();
	
?>