<?php
	
	class Socket{
		
		public $mssg = '';
		public $protocol = '';
		public $port = '';
		public $host = false;
		public $socket = false;
		public $response = '';
		public $proxy = false;
		public $connection = false;
		
		private $async = false;
		private $timeout = '';
		
		protected $error = false;

		public function __construct(){
			$this->error = array('number'=>false,'string'=>false);
		}
		
		public function getError(){
			return $this->error;
		}
		
		public function getResponse(){
			return $this->response;
		}
		
		public function setMessage($_str){
			$this->mssg = $_str;
		}
		
		public function setHost($_str){
			$this->host = $_str;
		}
		
		public function setProtocol($_str){
			$this->protocol = $_str;
		}
		
		public function setPort($_str){
			$this->port = $_str;
		}
		
		public function setTimeout($_str){
			$this->timeout = $_str;
		}
		
		public function setAsync($_bool){
			$this->async = $_bool;
		}
		
		public function setProxy($_host,$_port,$_scheme=false){
			if(!$_scheme) $_scheme = '';
			$this->proxy = array('host'=>$_host,'port'=>$_port,'protocol'=>$_scheme);
		}
		
		public function setError($_num,$_str){
			$this->error = array(
				'number'=>$_num,
				'string'=>$_str
			);
		}
		
		public function connect(){
			if(!$this->host) throw new Exception('Socket connection should have a host');
			if(!$this->port) throw new Exception('Socket connection should connect through a port');
			
			$this->connection = array();
			if($this->proxy){
				$this->connection['protocol'] = $this->proxy['protocol'] == 'https' ? 'ssl://' : ($this->proxy['protocol'] == 'udp' ? 'udp://' : '');
				$this->connection['host'] = $this->connection['protocol'] . $this->proxy['host'];
				$this->connection['port'] = $this->proxy['port'];
			}else{
				$this->connection['protocol'] = $this->protocol == 'https' ? 'ssl://' : ($this->protocol == 'udp' ? 'udp://' : '');
				$this->connection['host'] = $this->connection['protocol'] . $this->host;
				$this->connection['port'] = $this->port;
			}
			
			//start connect
			$this->socket = @fsockopen(
				$this->connection['host'],
				$this->connection['port'],
				$this->error['number'],
				$this->error['string']
			);
			
			return $this->socket;
		}
		
		public function send(){
			if($this->timeout) stream_set_timeout($this->socket,$this->timeout);
				
			$this->response='';
			if(!fputs($this->socket,$this->mssg)) throw new Exception('Cannot put request to server. fputs return 0.');
			
			//if asyncronous close connection, forget about the response and return true. used on loopback.
			if($this->async) {fclose($this->socket); return true;}
			
			//get response
			while(!feof($this->socket)){$this->response.=fgets($this->socket);}
			fclose($this->socket);
			
			//return something
			if($this->response) return $this->response;
			else return false;
		}
	}
	
?>
