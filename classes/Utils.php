<?php

require_once 'Vars.php';

class Utils{
	
	/* 
	*  dependencies (should be included before and outside this class):
	* 	- Vars.php
	*/
	
	public static function isHome(){
		return !sizeof(self::getUri());
	}

	public static function getSite($prot=false){
		if(isset(Vars::$GLOBAL['website'])) 
		return $prot ?  Vars::$protocol .'://'. Vars::$GLOBAL['website'] : Vars::$GLOBAL['website'];	
		Vars::$GLOBAL['website'] = Vars::$host . Vars::$root;
		return $prot ?  Vars::$protocol .'://'. Vars::$GLOBAL['website'] : Vars::$GLOBAL['website'];
	}
	
	public static function getUrl(){
		return 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	
	public static function getUri(){
		if(!isset(Vars::$GLOBAL['request'])) self::getRequest();
		return Vars::$GLOBAL['request']['uri'];
	}
	
	public static function getRequest(){
		if(isset(Vars::$GLOBAL['request'])) return Vars::$GLOBAL['request'];
		$url = parse_url(Vars::$protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		Vars::$GLOBAL['request'] = $url;
		Vars::$GLOBAL['request']['method'] = $_SERVER['REQUEST_METHOD'];
		
		Vars::$GLOBAL['request']['uri'] = explode('/', substr(Vars::$GLOBAL['request']['path'],
			strpos(Vars::$GLOBAL['request']['path'],Vars::$root)+strlen(Vars::$root)
		));
		
		if(!Vars::$GLOBAL['request']['uri'][0])
		array_splice(Vars::$GLOBAL['request']['uri'],0,1);
		if(count(Vars::$GLOBAL['request']['uri']) &&
		!Vars::$GLOBAL['request']['uri'][count(Vars::$GLOBAL['request']['uri'])-1])
		array_pop(Vars::$GLOBAL['request']['uri']);
		
		return Vars::$GLOBAL['request'];
	}
	
	public static function pp($str,$ret=0){
		$s =  '<pre>';
		$s .= print_r($str,true);
		$s .= '</pre>';
		if($ret) return $s;
		print $s;
		return true;
	}
	
	private static function cleanRec($arr){
		foreach($arr as $k=>&$v){
			if(is_array($v)) $v = self::cleanRec($v);
			else $v = stripslashes($v);
		}
		return $arr;
	}
	
	public static function clean($str){
		if(!isset(Vars::$GLOBAL['gpc'])) Vars::$GLOBAL['gpc'] = get_magic_quotes_gpc();
		
		if(Vars::$GLOBAL['gpc']){
			if(is_array($str)){
				$str = self::cleanRec($str);
			}else{
				$str = stripslashes($str);
			}
		}
		return $str;
	}
	
	public static function replaceBreak($str,$replace=''){
		$d = preg_replace('/\r\n/',"\n",$str);
		$d = preg_replace('/\r/',"\n",$d);
		return preg_replace('/\n/',$replace,$d);
	}
	
	public static function replaceHtmlBreak($str,$replace=''){
		$d = preg_replace('/<br\ \/>/',"\n",$str);
		$d = preg_replace('/<br\/>/',"\n",$d);
		return preg_replace('/<br>/',$replace,$d);
	}
	
	public static function metaRefresh($seconds,$urlR=''){
		if($urlR) $urlR = '; url='.$urlR;
		print "<meta http-equiv=\"refresh\" content=\"$seconds$urlR\">";
		return true;
	}
	
	public static function redirect($url,$meta=false){
		if($meta){
			self::metaRefresh(0,$url);
			die();
		}
		header('HTTP/1.1 303 See Other');
		header('Location: '.$url);
		die();
	}
	
	public static function stripKey($arr,$strip){
		$r = array();
		foreach($arr as $k=>&$v){
			if(is_array($v)) $v = self::stripKey($v,$strip);
			if(is_numeric($k)) {
				$r[$k] = $v;
			}else{
				$r[preg_replace('`'.preg_quote($strip).'`','',$k)] = $v;
			}
		}
		return $r;
	}
	
	public static function removeFirstBreak($str){
		$str = preg_replace('/^<p><br\ \/>/','<p>',$str);
		$str = preg_replace('/^<p><br\/>/','<p>',$str);
		$str = preg_replace('/^<p><br>/','<p>',$str);
		$str = preg_replace('/^<div><br\ \/>/','<div>',$str);
		$str = preg_replace('/^<div><br\/>/','<div>',$str);
		$str = preg_replace('/^<div><br>/','<div>',$str);
		$str = preg_replace('/^<br\ \/>/','',$str);
		$str = preg_replace('/^<br\/>/','',$str);
		$str = preg_replace('/^<br>/','',$str);
		return $str;
	}
	
	public static function arrayReset($src) {
		$dest = array();
		foreach ($src as $key => $value) {
			$dest[] = $value;
		}
		return $dest;
	}
	
	public static function validEmail($email){
		$atIndex = strrpos($email, "@");
		if (is_bool($atIndex) && !$atIndex) return false;
		
		$domain = substr($email, $atIndex+1);
		$local = substr($email, 0, $atIndex);
		$localLen = strlen($local);
		$domainLen = strlen($domain);
		if ($localLen < 1 || $localLen > 64) return false;
		if ($domainLen < 1 || $domainLen > 255) return false;
		if ($local[0] == '.' || $local[$localLen-1] == '.') return false;
		if (preg_match('/\\.\\./', $local)) return false;
		if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) return false;
		if (preg_match('/\\.\\./', $domain)) return false;
		if (
			!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)) &&
			!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))
		) return false;
		
		return true;
	}
	
	public static function dateToHuman($ds){
		if($ds=='0000-00-00 00:00:00') return 'never';
		$n = time();
		$d = strtotime($ds);
		return date('d F Y',$d);
	}
	
	public static function utf_8($r){
		if(is_array($r)){
			foreach($r as $k=>&$v){
				$v = self::utf_8($v);
			}
			return $r;
		}
		
		return utf8_encode($r);
	}
}
	
?>
