<?php
class DB{
		/* class to execute db query, contain some security-related stuff (verifications);
		 *  dependencies (should be included before and outside this class):
		 * 	- Vars.php
		 *	
		 * 	replace domain by giberish on "clean" to ease up migration
		 * 	replace giberish by domain on "setutf" to ease up migration
		 */
		
		public $pointer = false;
		private $dbname = '';
		private $username = '';
		private $password = '';
		private $host = '';
		private $domain = '';
		private $port = 3306;
		private $domainString = 'hwof3456lsjdfj--';
		private $domainStringEsc = 'vzjdfr82733i7e--';
		private $lastquery = '';
		private $err = '';
		private $charset = 'utf8';
		
		
		// memcache settings
		private $mcache = false;
		private $mcachehost = '127.0.0.1';
		private $mcacheport = 11211;
		private $mcache_d = false;
		private $mcacheExpire = 30; //seconds
		private $mcacheServer;
		private $mcacheUse = false;
		
		public function __construct($dbname,$username='root',$password='',$host='127.0.0.1',$port=3306){
			
			$this->dbname = $dbname;
			$this->username =  $username;
			$this->password = $password;
			$this->host = $host;
			$this->port = $port;

			$host = $_SERVER['HTTP_HOST'];
			$root = str_replace($_SERVER['DOCUMENT_ROOT'],'',preg_replace('/\/[^\/]+$/','',$_SERVER['SCRIPT_FILENAME']));
			$root = preg_replace('/\/+/','/','/'.$root.'/');
			$protocol = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's');
			$this->domain = $protocol . '://' . $host . $root;
			
			// fill in your memcache server
			// array('server'=>'123.123.123.123','port'=>11211);
			$this->mcacheServer = array();
			
		}

		public function setDomain($domain){
			$this->domain = $domain;
		}
		
		public function useMemcache($b=true){
			if(!$b) $this->mcacheUse = false;
			else $this->mcacheUse = true;
		}
		
		private function mget($query){
			if(!$this->mcache || !$this->mcacheUse) return false;
			return $this->mcache->get('mysql-'.$query);
		}
		
		private function mset($query,$res){
			if(!$this->mcache || !$this->mcacheUse) return false;
			if($this->mcache_d){
				return $this->mcache->set('mysql-'.$query,$res,$this->mcacheExpire);
			}else{
				return $this->mcache->set('mysql-'.$query,$res,0,$this->mcacheExpire);
			}
		}
		
		private function initMcache(){
			//initiate memcache
			if(class_exists('Memcached')){
				$this->mcache = new Memcached();
				$this->mcache_d = true;
				$this->mcache->addServer($this->mcachehost,$this->mcacheport);
			}else if(class_exists('Memcache')){
				$this->mcache = new Memcache;
				$this->mcache->connect($this->mcachehost,$this->mcacheport);
			}else{
				$this->mcache = false;
				$this->mcacheUse = false;
			}
			
			foreach($this->mcacheServer as $k=>$v){
				$this->mcache->addServer($v['server'],$v['port']);
			}
			
			if($this->mcache_d){
				$this->mcache->setOptions(array(
					Memcached::OPT_COMPRESSION=>true,
					Memcached::OPT_SERIALIZER=>Memcached::SERIALIZER_PHP,
					Memcached::OPT_LIBKETAMA_COMPATIBLE=>true
				));
			}else if($this->mcache){
				$this->mcache->setCompressThreshold(100);
			}
		}
		
		public function error(){
			return $this->err ? $this->err : mysqli_error($this->pointer);
		}
		
		public function setError($str=''){
			if(!$this->pointer) {
				$this->err = 'Cannot connect to DBMS. verify your username and password.';
				return;
			}
			$this->err = $str ? $str : mysqli_error($this->pointer);
		}
		
		public function lastQuery(){
			return $this->lastquery;
		}
		
		public function getPointer(){
			if(!$this->pointer) return $this->init();
			else return $this->pointer;
		}
				
		public function init(){
			if($this->pointer) return $this->pointer;
			$this->pointer = @mysqli_connect($this->host,$this->username,$this->password,$this->dbname,$this->port);
			//$this->pointer = @mysqli_connect($this->host,$this->username,$this->password);
			
			$this->initMcache();
			mysqli_set_charset($this->pointer,$this->charset);

			//return mysql pointer
			if(!$this->pointer) {$this->setError(); return false;}
			return $this->pointer;
		}
		
		public function query($str){
			
			//Utils::pp($str);
			$this->lastquery = $str;
			
			if(!$this->pointer && !$this->init()) return false;
			$r = @mysqli_query($this->pointer,$str);
			if(!$r) {
				$this->setError();
				return false;
			}
			return $r;
		}
		
		public function multiQuery($str){
			
			//Utils::pp($str);
			$this->lastquery = $str;
			
			if(!$this->pointer && !$this->init()) return false;
			$r = @mysqli_multi_query($this->pointer,$str);
			
			if(!$r) {
				$this->setError();
				return false;			}			
			$result = false;
			$res = array();
			do{
				if ($result = mysqli_store_result($this->pointer)) {
					$ar = array();
        		    while ($row = mysqli_fetch_assoc($result)) { $ar[] = $row; }
        		    if(sizeof($ar)) $res[] = $ar;
        		    mysqli_free_result($result);
        		}
			} while (mysqli_more_results($this->pointer) && mysqli_next_result($this->pointer));
			if (mysqli_errno($this->pointer)) {$this->setError();return false;}
			
			return $this->setutf($res);
			
		}
		
		public function insert($table,$data){
			
			//$ver = $verify
			if(is_array($data)){
				$dat = array();
				$fie = array();
				foreach($data as $k=>$v) {
					$dat[] = $this->format($v);
					$fie[] = "`".$this->clean($k)."`";
				}
				$dat = implode(',',$dat);
				$fie = implode(',',$fie);
			} else {
				throw new exception("DB class needs an array on insert");
			}
			return $this->query("insert into $table ($fie) values ($dat)");
		}
		
		public function update($table,$data,$where=false){
			
			//$ver = $verify
			if(is_array($data)){
				$dat = array();
				foreach($data as $k=>$v) {
					$k = $this->clean($k);
					$dat[] = "`$k`=".$this->format($v);
				}
				$dat = implode(',',$dat);
			}else{ $dat = $data;}
			
			if(is_array($where)){
				$wh = array();
				foreach($where as $k=>$v){
					if(is_numeric($k)) {$wh[] = $v;continue;}
					$k = $this->clean($k);
					if(is_array($v)){
						if(sizeof($v)=='3'){
							$v = $v[2].' '.$this->format($v[0]).' and '.$this->format($v[0]);
							$wh[] = "`$k` ".$v;
						}else{
							$wh[] = "`$k` ".$v[1]." ".$this->format($v[0]);
						}
					}else{
						$wh[] = "`$k`=".$this->format($v);
					}
				}
				$wh = implode(' and ',$wh);
			}else{$wh = $where ? $where : '1';}
			
			
			//print "update $table set $dat where $where";
			return $this->query("update $table set $dat where $wh");
		}
		
		public function select($fields,$table,$where=1,$group='',$having='',$order='',$limit=''){
			
			//$ver = $verify
			if($group) $group=' group by '.$group;
			if($having) $having=' having '.$having;
			if($order) $order=' order by '.$order;
			if($limit) $limit=' limit '.$limit;
			if(is_array($fields)) {
				foreach($fields as &$fi) if(!preg_match('/ as /',$fi)) $fi = "`$fi`";
				$fields = implode(',',$fields);
			}
			
			if(is_array($where)) {
				$e = array();
				foreach($where as $k=>&$v){
					if(is_numeric($k)) {$e[] = $v;continue;}
					$k = $this->clean($k);
					if(is_array($v)){
						if(sizeof($v)=='3'){
							$v = $v[2].' '.$this->format($v[0]).' and '.$this->format($v[0]);
							$e[] = "`$k` ".$v;
						}else{
							$e[] = "`$k` ".$v[1]." ".$this->format($v[0]);
						}
					}else{
						$e[] = "`$k`=".$this->format($v);
					}
				}
				$where = implode(' and ',$e);
			}

			//print "select $fields from $table where $where$group$having$order$limit";
			$q = "select $fields from $table where $where$group$having$order$limit";
			
			//use memcache
			$res = $this->mget($q);
			if($res) return json_decode($res,true);
			$res = $this->fetch($this->query($q));
			if($res) $this->mset($q,json_encode($res));
			return $res;
		}
		
		public function delete($table,$where){
			
			//$ver = $verify
			//file_put_contents('error.log', "delete from $table where $where",FILE_APPEND);
			if(is_array($where)) {
				$e = array();
				foreach($where as $k=>&$v){
					if(is_numeric($k)) {$e[] = $v;continue;}
					$v = $this->clean($v);
					$e[] = "`".$this->clean($k)."`=".$this->format($v);
				}
				$where = implode(' and ',$e);
			}

			return $this->query("delete from $table where $where");
		}
		
		public function fetch($pointer,$array=0){
			if(!$pointer) return false;
			if(is_array($pointer)) return $pointer;
			
			$d = array();
			if($array){
				while($r = mysqli_fetch_array($pointer,MYSQLI_NUM)) $d[] = $r;
			}else{
				while($r = mysqli_fetch_assoc($pointer)) $d[] = $r;
			}
			
			if(sizeof($d)) return $this->setutf($d);
			return $d;
		}

		public function format($str){
			if(is_array($str)) return $str[0];
			if(is_int($str) || is_float($str)) return $str;
			return "'".$this->clean($str)."'";
		}

		public function clean($str){
			if(!$this->pointer && !$this->init()) return false;
			if(get_magic_quotes_gpc()) $str = stripslashes($str);
			if(is_array($str)){
				foreach ($str as $key => &$value) {
					$value = self::clean($value);
				}
			}else{
				$str = mysqli_real_escape_string($this->pointer,$this->replaceDomain($this->cleanUTF($str)));
			}
			return $str;
		}
		
		private function setutf($array){
			foreach($array as $k=>&$v){
				if(is_string($v)) $v = $this->addDomain($this->cleanUTF($v));
				if(is_array($v)) $v = $this->setutf($v);
			}
			return $array;
		}
		
		private function addDomain($str){
			$str = preg_replace('`'.preg_quote($this->domainString).'`',$this->domain,$str);
			//json escaped characters
			$str = str_replace('`'.preg_quote($this->domainStringEsc).'`',preg_replace('/\//',"\\\/",$this->domain),$str);
			return $str;
		}
		
		private function replaceDomain($str){
			$str = str_replace($this->domain,$this->domainString,$str);
			//json escaped characters
			$str = str_replace(preg_replace('/\//',"\\\/",$this->domain),$this->domainStringEsc,$str);
			
			return $str;
		}

		private function cleanUTF($some_string){
			//reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
			$some_string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
			 '|[\x00-\x7F][\x80-\xBF]+'.
			 '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
			 '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
			 '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
			 '', $some_string );
			 
			//reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
			$some_string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
			 '|\xED[\xA0-\xBF][\x80-\xBF]/S','', $some_string );

			return $some_string;
		}
		
	}
?>