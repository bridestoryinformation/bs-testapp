<?php
	include '_includes.php';
?><!DOCTYPE html>
<html>
	<head>
		<noscript><meta http-equiv="refresh" content="0; url=noscript.php" /></noscript>
		<title>Browser Incompatible</title>
		<?php $HOST = Utils::getSite(true); ?>	
		<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/3.0.2/normalize.css">
		<style>
			
			html, button, input, select, textarea{
				font-family: "Open Sans", "sans serif";
				color:#444;
			}

			body{
				font-size:13px;
				line-height:21px;
			}
			
			#logo{
				font-size:34px;
				margin-top:0px;
				line-height:55px;
			}
			
			a{
				text-decoration:none;
				color:#0078e7;
				display:inline-block;
				*display:inline;
				width:128px;
				height:165px;
				padding:13px 0;
				text-align:center;
				font-size:16px;
				color:#999999;
				margin-right:21px;
				border-bottom:1px solid transparent;
			}
			
			a img{border:none;width:128px;}
			a .name{
				margin-top:13px; 
			}
			
			a:hover{
				border-bottom:2px solid #dedede;
			}

			.label{
				display:inline-block;
				padding:3px 8px;
				background-color:red;
				color:white;
				margin:21px 0;
				font-weight:bold;
			}
		</style>
	</head>
	<body>
		<div style="padding:34px;">
		<p id="logo"><?php print Vars::$appName; ?></p>
		<p class="label">Error: incompatible browser</p>
		<p>
			You are using an incompatible browser.
			To gain full functionality of this cms,<br />
			Please use one of the following browser(s):
		</p>
		<br />
		<br />
		<a class="browser" href="http://www.google.com/chrome">
			<img src="https://lh5.googleusercontent.com/-64bTZeZHn-E/UpJ21TEFMhI/AAAAAAAABNA/N_4WLksVLdY/s128/1385351821_Chrome.png" />
			<div class="name">Chrome 25+</div>
		</a>
		
		<a class="browser" href="http://www.firefox.com/">
			<img src="https://lh3.googleusercontent.com/-kxOmxIJvD9M/UpJ3Pk1GL3I/AAAAAAAABNk/tQe548rOE88/s128/1385352056_firefox-icon.png" />
			<div class="name">Firefox 20+</div>
		</a>
		
		<a class="browser" href="http://www.apple.com/safari/">
			<img src="https://lh4.googleusercontent.com/-sgUq_scHtWo/UpJ23ICNeQI/AAAAAAAABNY/1VBE_yVVuc0/s128/1385351933_Safari.png" />
			<div class="name">Safari 6+</div>
		</a>
		
		<a class="browser" href="http://www.opera.com/">
			<img src="https://lh6.googleusercontent.com/-EqhxCWVcZnw/UpJ20DfbKyI/AAAAAAAABM4/rbczv4AnEIs/s128/1385351799_Opera.png" />
			<div class="name">Opera 12+</div>
		</a>
		
		<a class="browser" href="http://windows.microsoft.com/ie">
			<img src="https://lh5.googleusercontent.com/-J33Qdjl4ub0/UpJ22kMgfsI/AAAAAAAABNU/QudreiZ6LO4/s128/1385351925_IE.png" />
			<div class="name">IE 10+</div>
		</a>
	</body>
</html>
