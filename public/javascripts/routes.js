
// default page hash
App.defaultHash = '#default';

// hash routes
App.setRoute({
    url: /^default$/,
    page: 'default.html'
});

App.setRoute({
    url: /^docs$/,
    page:'docs.html'
});

App.setRoute({
    url: /^api$/,
    page:'api.html'
});

// using callback example
App.setRoute({
    url: /^ref$/,
    callback: function(){
        App.ui.loader(true);
        App.page.load('reference.html')
        .done(function(d){
            App.page.setContent(d);
            $('pre code').each(function(i, block) { hljs.highlightBlock(block); });
            App.event.trigger('templateloaded');
            App.ui.loader(false);
        }).fail(function(e){
            App.loader(false);
            App.ui.modal.error(e.responseJSON?e.responseJSON.message:(e.responseText||'Unknown error'));
            App.event.trigger('templateloaderror',e.responseJSON?e.responseJSON.message:(e.responseText||'Unknown error'));
        });
    }
});