(function(){

App.ui = {};
App.ui.loader = function(n){
	if(n) $('#loader').addClass('on');
	else $('#loader').removeClass('on');
};

App.page = {};
App.page.load = function(url){
	return $.ajax({ method:'get', url: App.getSite()+'pages/'+url, dataType:'text' });
};

App.page.setContent = function(content){ $('.container-fluid').html(content); };

(function(){
 
	var Event = {};
	var regRep = function(str){return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");};
	var execFunc = function(f,d){ if(typeof f == 'string') window[f](d); else f(d); };
	
	Event.list = {};
 
 
	// turn on event, support multiple handlers
	// support namespace by dot (.) ex: eventname.default
	// multiple level of namespace: eventname.default.fromhim
	Event.on = function(str,call){
		if(!str) return;
		if(typeof Event.list[str] == 'undefined') Event.list[str] = [];
		Event.list[str].push(call);
	};
 
 
	// turn off, based on callback, or namespace, or event name
	// turn off by namespace: all child events will be deleted 
	// ex: "Event.off( eventname.default )" will delete eventname.default and eventname.default.fromhim
	Event.off = function(str,call){
		if(typeof Event.list[str] == 'undefined') return;
		if(call) {
			for(var i in Event.list[str]) {
				if(Event.list[str][i] === call) delete Event.list[str][i];
			}
			return;
		}else{
			var r1 = new RegExp('^'+regRep(str)+'$');
			var r2 = new RegExp('^'+regRep(str)+'\.');
			for(var i in Event.list){
				if(i.match(r1) || i.match(r2)) delete Event.list[i];
			}
		}	
	};
 
 
	// trigger event, the "data" will be passed to handler as parameters
	// for namespaced events: 
	// "eventname" will trigger "eventname" & "eventname.default" & "eventname.default.fromhim"
	// "eventname.default" will trigger "eventname.default" & "eventname.default.fromhim"
	Event.trigger = function(str,data){
		var r1 = new RegExp('^'+regRep(str)+'$');
		var r2 = new RegExp('^'+regRep(str)+'\.');
		for(var i in Event.list){
			if(i.match(r1) || i.match(r2)) {
				for(var j in Event.list[i]) {
					execFunc( Event.list[i][j], data );
				}
			}
		}
	};
 
 
	// use this to create interface in global context
	App.event = {
		on: Event.on,
		off: Event.off,
		trigger: Event.trigger
	};
 
})();


(function(){
	var Sitedata = {};
	App.data = function(){
		if(arguments.length==1 && typeof Sitedata[arguments[0]] != 'undefined') return Sitedata[arguments[0]];
		else if(arguments.length>1) Sitedata[arguments[0]] = arguments[1];
		else return null;
	};
})();

App.api = function(){
	var meth = 'get',url='',data=undefined;
	if(arguments.length==3){
		meth = arguments[0];
		url = arguments[1];
		data = arguments[2];
	}

	if(arguments.length==2){
		meth = arguments[0];
		url = arguments[1];
	}

	if(arguments.length==1){
		url = arguments[0];
	}

	return $.ajax({
		method:meth.toUpperCase(),
		url: App.apiUrl()+url,
		data:data
	});	
};

App.ui.modal = {
	ok: function(message){
		bootbox.dialog({
			message: message,
			title: '<span class="ok">Success</span>',
			buttons:{
				main: {
			      label: "Close",
			      className: "btn-primary"
			    }
			}
		});
	},
	error: function(message){
		bootbox.dialog({
			message: message,
			title: '<span class="error">Error</span>',
			buttons:{
				main: {
			      label: "Close",
			      className: "btn-primary"
			    }
			}
		});
	},
	confirm: function(message,callback){
		bootbox.confirm({
			message: message,
			title: '<span class="ok">Confirm</span>',
			callback: function(res) { callback(res) }
		});
	},
	custom: function(opt){
		bootbox.dialog(opt);
	}
};

$(function(){
	var currenthash = window.location.hash;
	App.page.hash = function(){
		return currenthash;
	};

	var hashchange = function(url){
		if(!url) {
			// default url
			window.location.hash = App.defaultHash;
			return;
		}
		currenthash = url;
		var h = App.getRoute(url);
		if(!h) return true;
		if(h.callback) {
			App.event.trigger('hashload');
			h.callback(url);
		}else{
			App.event.trigger('hashload');
			App.ui.loader(true);
			App.page.load(h.page)
			.done(function(d){
				window.scrollTo(0, 0);
				App.page.setContent(d);
				$('pre code').each(function(i, block) { hljs.highlightBlock(block); });
				App.event.trigger('pageloaded');
				App.ui.loader(false);
			}).fail(function(e){
				window.scrollTo(0, 0);
				App.ui.loader(false);
				App.ui.modal.error(e.responseJSON?e.responseJSON.message:(e.responseText||'Unknown error'));
				App.event.trigger('pageloaderror',e.responseJSON?e.responseJSON.message:(e.responseText||'Unknown error'));
			});
		}
	};

	window.onhashchange = function(){
		hashchange(window.location.hash.replace(/^\#/,''));
		$('.side-menu').removeClass('active');
		var nav = window.location.hash;
		var nav_arr = nav.split('/');
		if(nav_arr.length>2){
			nav = nav.replace('/'+nav_arr[nav_arr.length-1], '');
		}
		//console.log(nav);
		$('.side-menu[href="'+nav+'"]').addClass('active');
	};
	window.onhashchange();	
});

})();