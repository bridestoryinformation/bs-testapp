$(function() {

    $('#side-menu').metisMenu();

    $('.side-menu').on('click', function(){ 
        if($(this).attr('href') != '#'){
            $('.sidebar-nav').attr('aria-expanded', false).removeClass('in')
        }
    })
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});



function setError(m){
    App.modal.error(m);
    App.loader(false);
}

String.prototype.toProperCase = function(){
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

var twoDigits = function(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

var parseDate = function(date_str){
    // will parse dd-mm-yyy to Date Object
    if(typeof date_str == "object") return date_str;
    var arr = date_str.split('-');
    if(arr.length!=3) return false;
    return new Date(arr[2], arr[1]-1, arr[0]);
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

Date.prototype.toHumanDate = function() {
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate());
};

Date.prototype.toHumanDate = function() {
    return twoDigits(this.getDate()) + "-" + twoDigits(1 + this.getMonth()) + "-" + this.getFullYear();
};

Date.prototype.toHumanDateTime = function() {
    return twoDigits(this.getDate()) + "-" + twoDigits(1 + this.getMonth()) + "-" + this.getFullYear() + " " +
    twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
};