<?php
	include '_includes.php';
?><!DOCTYPE html>
<html>
	<head>
		<title>Javascript Disabled</title>
		<?php $HOST = Utils::getSite(true); ?>
		<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/3.0.2/normalize.css">
		<style>
			
			html, button, input, select, textarea{
				font-family: "Open Sans", "sans serif";
				color:#444;
			}
			input, select, textarea{
				background-color:white;
				color:#444;
			}

			body{
				font-size:13px;
				line-height:21px;
			}
			
			#logo{
				font-size:34px;
				margin-top:0px;
				line-height:55px;
			}
			
			a{
				text-decoration:none;
				color:#0078e7;
			}
			
			#left{
				height:100%;
				width:30%;
				position:fixed;
				top:0;
				left:0;
				padding:34px;
				box-sizing:border-box;
				-moz-box-sizing:border-box;
				-webkit-box-sizing:border-box;
				background-color:#F0F0F0;
				border-right:1px solid #cccccc;
			}
			
			iframe{
				border:none;
				height:100%;
				width:70%;
				position:fixed;
				top:0;
				right:0;
			}

			.label{
				display:inline-block;
				padding:3px 8px;
				background-color:red;
				color:white;
				margin:21px 0;
				font-weight:bold;
			}
		</style>
	</head>
	<body>
		<div id="left">
		<p id="logo"><?php print Vars::$appName; ?></p>
		<p class="label label-error">Error: JavaScript is not enabled</p>
		<p>For full functionality of this site, it is necessary to enable JavaScript.
			<br /><br />
			Read the instructions on the right to enable JavaScript.
			<br /><br />
			If you can't see it, Here are the <a href="http://www.enable-javascript.com/">
			instructions how to enable JavaScript in your web browser</a>.
			<br /><br /><br /><br />
			<a href="login.php">&laquo; Go to login page</a> if you're done.
		</p>
		</div>
		<iframe src="http://www.enable-javascript.com/"></iframe>
	</body>
</html>
